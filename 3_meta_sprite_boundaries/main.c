#include <gb/gb.h>
#include <gb/console.h>
#include <gb/font.h>
#include <stdio.h>
#include "meta_ship.c"
#include "1_meta_sprite_ship.c"

struct MetaShip ship;

const UBYTE spriteSize = 8;
const UINT8 xMin = 8;
const UINT8 xMax = 152;
const UINT8 yMin = 17;
const UINT8 yMax = 128;

UINT8 i;
UINT8 movementStep = 3;

void performantDelay(UINT8 numloops) {
    for (i = 0; i < numloops; i++) {
        wait_vbl_done();
    }
}

void moveShip(struct MetaShip* ship, UINT8 x, UINT8 y) {
    move_sprite(ship->spriteIds[0], x, y);
    move_sprite(ship->spriteIds[1], x + spriteSize, y);
    move_sprite(ship->spriteIds[2], x, y + spriteSize);
    move_sprite(ship->spriteIds[3], x + spriteSize, y + spriteSize);
}

void setupShip() {
    // set initial coords and dimensions
    ship.x = 80;
    ship.y = 110;
    ship.width = 16;
    ship.height = 16;

    // load sprites
    for (i = 0; i <= 3; i++) {
        set_sprite_tile(i, i);
        ship.spriteIds[i] = i;
    }

    moveShip(&ship, ship.x, ship.y);
}

void main() {
    font_t min_font;
    font_init();
    min_font = font_load(font_ibm_fixed);
    font_set(min_font);

    printf("\n \n \n---  METASHIP    ---");
    printf("\n \n \n---  GBDK 2020   ---");
    printf("\n \n \n \n \npress A to continue");
    waitpad(J_A);

    cls();
    gotoxy(0, 0);

    set_sprite_data(0, 4, metaShip);
    setupShip();

    SHOW_SPRITES;
    DISPLAY_ON;

    gotoxy(0, 16);
    setchar('x');
    gotoxy(0, 17);
    setchar('y');

    while(1) {
        gotoxy(2, 16);
        printf("   ");
        gotoxy(2, 17);
        printf("   ");
        if ((joypad() & J_LEFT) && ship.x > xMin) {
            ship.x -= movementStep;
            moveShip(&ship, ship.x, ship.y);
        }
        if ((joypad() & J_RIGHT) && ship.x < xMax) {
            ship.x += movementStep;
            moveShip(&ship, ship.x, ship.y);
        }
        if ((joypad() & J_UP) && ship.y > yMin) {
            ship.y -= movementStep;
            moveShip(&ship, ship.x, ship.y);
        }
        if ((joypad() & J_DOWN) && ship.y < yMax) {
            ship.y += movementStep;
            moveShip(&ship, ship.x, ship.y);
        }
        gotoxy(2, 16);
        printf("%d", ship.x);
        gotoxy(2, 17);
        printf("%d", ship.y);

        performantDelay(5);
    }
    
}