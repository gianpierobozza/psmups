#include <gb/gb.h>

struct MetaShip {
    UBYTE spriteIds[4]; //4 sprites per struct
    UINT8 x;
    UINT8 y;
    UINT8 width;
    UINT8 height;
};
