/*

 1_META_SPRITE_SHIP.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 0 to 3

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char metaShip[] =
{
  0x01,0x01,0x02,0x03,0x07,0x07,0x05,0x05,
  0x08,0x08,0x09,0x08,0x12,0x11,0x14,0x13,
  0x80,0x80,0x40,0xC0,0xE0,0xE0,0xA0,0xA0,
  0x10,0x10,0x90,0x10,0x48,0x88,0x28,0xC8,
  0x22,0x21,0x21,0x20,0x24,0x20,0x70,0x74,
  0x50,0x74,0x75,0x71,0x21,0x21,0x1F,0x1F,
  0x44,0x84,0x84,0x04,0x24,0x04,0x0E,0x2E,
  0x0A,0x2E,0xAE,0x8E,0x84,0x84,0xF8,0xF8
};

/* End of 1_META_SPRITE_SHIP.C */
