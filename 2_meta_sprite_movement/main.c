#include <gb/gb.h>
#include <stdio.h>
#include "meta_ship.c"
#include "1_meta_sprite_ship.c"

struct MetaShip ship;
const UBYTE spriteSize = 8;
UINT8 i;

void performantDelay(UINT8 numloops) {
    for (i = 0; i < numloops; i++) {
        wait_vbl_done();
    }
}

void moveShip(struct MetaShip* ship, UINT8 x, UINT8 y) {
    move_sprite(ship->spriteIds[0], x, y);
    move_sprite(ship->spriteIds[1], x + spriteSize, y);
    move_sprite(ship->spriteIds[2], x, y + spriteSize);
    move_sprite(ship->spriteIds[3], x + spriteSize, y + spriteSize);
}

void setupShip() {
    // set initial coords and dimensions
    ship.x = 80;
    ship.y = 130;
    ship.width = 16;
    ship.height = 16;

    // load sprites
    for (i = 0; i <= 3; i++) {
        set_sprite_tile(i, i);
        ship.spriteIds[i] = i;
    }

    moveShip(&ship, ship.x, ship.y);
}

void main() {

    printf("\n \n \n---  METASHIP    ---");
    printf("\n \n \n---  GBDK 2020   ---");
    printf("\n \n \n \n \npress A to continue");
    waitpad(J_A);

    HIDE_BKG;
    DISPLAY_OFF;

    set_sprite_data(0, 4, metaShip);
    setupShip();

    SHOW_SPRITES;
    DISPLAY_ON;

    while(1) {
        if (joypad() & J_LEFT) {
            ship.x -= 3;
            moveShip(&ship, ship.x, ship.y);
        }
        if (joypad() & J_RIGHT) {
            ship.x += 3;
            moveShip(&ship, ship.x, ship.y);
        }
        if (joypad() & J_UP) {
            ship.y -= 3;
            moveShip(&ship, ship.x, ship.y);
        }
        if (joypad() & J_DOWN) {
            ship.y += 3;
            moveShip(&ship, ship.x, ship.y);
        }

        performantDelay(5);
    }
    
}