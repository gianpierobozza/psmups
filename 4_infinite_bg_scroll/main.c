#include <gb/gb.h>
#include <gb/console.h>
#include <gb/font.h>
#include <stdio.h>
#include "2_space_background.c"
#include "2_space_background_map.c"

UINT8 i, j, key;
INT8 bkgSpeed;

void performantDelay(UINT8 numloops) {
    for (i = 0; i < numloops; i++) {
        wait_vbl_done();
    }
}

void main() {
    font_t min_font;
    font_init();
    min_font = font_load(font_min);
    font_set(min_font);

    printf("\n \n \n SCROLLING          ");
    printf("\n       BACKGROUND   ");
    printf("\n \n \n     GBDK 2020      ");
    printf("\n \n \n \npress A to continue");
    waitpad(J_A);

    cls();
    gotoxy(0, 0);

    bkgSpeed = 0;
    set_bkg_data(0, 16, spaceBkg);
    for (i = 0; i <= 7; i++) {
        for (j = 0; j <= 7; j++) {
            set_bkg_tiles(i*4, j*4, 4, 4, spaceBkgMap);
        }
    }
    move_bkg(49, 111);

    SHOW_BKG;
    DISPLAY_ON;

    while (TRUE) {
        key = joypad();

        if ((key & J_LEFT) && bkgSpeed > -6)
            bkgSpeed--;
        if ((key & J_RIGHT) && bkgSpeed < 6)
            bkgSpeed++;
        if ((key & J_LEFT) == FALSE && (key & J_RIGHT) == FALSE) {
            if (bkgSpeed < 0)
                bkgSpeed ++;
            else if (bkgSpeed > 0)
                bkgSpeed --;
        }

        scroll_bkg(bkgSpeed, -1);
        performantDelay(5);        
    }
    
}