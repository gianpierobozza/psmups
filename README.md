# Project SMUPS #

Learning GB Dev with a smups game.

### What is this repository for? ###

* Using the library GBDK 2020 I'm learning game development and C programming making an incremental "game" of shoot'em ups.
* pre-pre-alpha-1

### How do I get set up? ###

* Download [GBDK 2020](https://github.com/Zal0/gbdk-2020) and configure it.
* Emulator used for basic rom loading is [bgb](https://bgb.bircd.org/)
* For testing and debugging use the [https://emulicious.net/](Emulicious) emulator.
* Suggested IDE is [MS Visual Studio Code](https://code.visualstudio.com/) with the debugger plugin.
* [Emulicious Debugger](https://marketplace.visualstudio.com/items?itemName=emulicious.emulicious-debugger) link.
* Guide on how to setup the debugger can be found on this [Youtube video](https://www.youtube.com/watch?v=rM9gepdesJs) - credits to [Brian Norman](https://github.com/gingemonster).
* [Gameboy Tile Designer](http://www.devrs.com/gb/hmgd/gbtd.html) to design sprites/tiles and export them as C files.
* [Gameboy Map Builder](http://www.devrs.com/gb/hmgd/gbmb.html) to create maps made of tiles and export them as C files.
* profit
